from typing import List

from sqlalchemy.orm import Session
from sqlalchemy.exc import DatabaseError
from fastapi import APIRouter, Depends, HTTPException

from movies import schemas, models
from movies.dependencies import get_db
from movies.logs import get_logger


router = APIRouter()
log = get_logger(__name__)


@router.get("/", response_model=List[schemas.Category])
def get_categories_list(db: Session = Depends(get_db)):
    """
    Gets the categories list
    """
    categories = db.query(models.Category).all()
    return categories


@router.post("/", response_model=schemas.Category)
def create_category(
    category_data: schemas.NewCategoryRequest, db: Session = Depends(get_db)
):
    """
    Creates a new category
    """
    log.info("Category: %s", category_data.name)
    category = models.Category(name=category_data.name)
    try:
        db.add(category)
        db.commit()
    except DatabaseError as exc:
        log.exception("Could not save category", exc_info=exc)
        db.rollback()
        raise HTTPException(400, "Could not save category")
    return category


@router.get("/{category_id}", response_model=schemas.Category)
def get_category_details(category_id: int, db: Session = Depends(get_db)):
    """
    Get details for single category
    """
    category = db.query(models.Category).get(category_id)
    if category is None:
        raise HTTPException(404, "Category not found.")
    return category


@router.patch("/{category_id}", response_model=schemas.Category)
def modify_category(
    category_id: int,
    category_data: schemas.NewCategoryRequest,
    db: Session = Depends(get_db),
):
    category = db.query(models.Category).get(category_id)
    if category is None:
        raise HTTPException(404, "Category not found.")

    try:
        category.name = category_data.name
        db.commit()
    except DatabaseError as exc:
        log.error("Could not update category.", exc_info=exc)
        db.rollback()
        raise HTTPException(status_code=400, detail="Could not update category.")

    return category


@router.delete("{category_id}")
def delete_category(category_id: int, db: Session = Depends(get_db)):
    try:
        affected_rows = (
            db.query(models.Category).filter(models.Category.id == category_id).delete()
        )
        db.commit()
    except DatabaseError as exc:
        log.error("Could not delete category.", exc_info=exc)
        db.rollback()
        raise HTTPException(status_code=400, detail="Could not delete category.")

    if affected_rows == 0:
        raise HTTPException(status_code=404, detail="Category does not exist.")

    return {"message": "Category removed successfully."}
